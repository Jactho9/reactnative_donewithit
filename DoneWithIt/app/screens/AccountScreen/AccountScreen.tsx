import React from 'react';
import {FlatList, View} from "react-native";
import Screen from "../../components/Screen";
import ListItem from "../../components/lists/ListItem";
import {Colours} from "../../config/colours";
import Icon from "../../components/Icon/Icon";
import Styles from "./styles";
import ListItemSeparator from "../../components/lists/ListItemSeparator";
import {useNavigation} from "@react-navigation/native";
import {useAuth} from "../../auth/useAuth";

const menuItems = [
    {
        title: "My Listings",
        icon: {
            name: "format-list-bulleted",
            backgroundColor: Colours.primary
        }
    },
    {
        title: "My Messages",
        icon: {
            name: "email",
            backgroundColor: Colours.secondary
        },
        targetScreen: "Messages"
    }
]

const AccountScreen = () => {
    const navigation = useNavigation();
    const {logout, user} = useAuth();

    return (
        <Screen style={Styles.screen}>
            <View style={Styles.container}>
                <ListItem
                    title={user?.name}
                    subTitle={user?.email}
                    image={require("../../assets/mosh.jpg")}
                />
            </View>
            <View style={Styles.container}>
                <FlatList
                    data={menuItems}
                    keyExtractor={menuItem => menuItem.title}
                    ItemSeparatorComponent={ListItemSeparator}
                    renderItem={({item}) =>
                        <ListItem
                            title={item.title}
                            IconComponent={
                                <Icon name={item.icon.name} backgroundColor={item.icon.backgroundColor}/>
                            }
                            onPress={() => item.targetScreen && navigation.navigate(item.targetScreen)}
                        />
                    }
                />
            </View>
            <ListItem
                title="Log Out"
                subTitle={user?.email}
                IconComponent={<Icon name="logout" backgroundColor="#ffe66d" />}
                onPress={() => logout()}
            />
        </Screen>
    );
};

export default AccountScreen;