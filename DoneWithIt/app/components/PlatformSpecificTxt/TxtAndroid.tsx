import React from 'react';
import {Text, StyleSheet} from "react-native";


interface Props {
    children?: React.ReactNode
}

const TxtAndroid = ({children} : Props) => {
    return (
        <Text style={styles.text}>{children}</Text>
    );
};

const styles = StyleSheet.create({
    text:{
        color: "green",
        fontSize: 18,
        fontFamily: "Roboto"
    }
});

export default TxtAndroid;