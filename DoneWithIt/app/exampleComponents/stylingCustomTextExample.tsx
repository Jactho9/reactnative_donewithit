import React from 'react';
import {View} from "react-native";
import Txt from "../components/Txt/Txt";
import PlatformSpecificTxt from "../components/PlatformSpecificTxt/PlatformSpecificTxt";

const StylingCustomTextExample = () => {
    return (
        <View style={{
            flex: 1,
            alignItems: "center",
            justifyContent: "center",
        }}>
            <Txt>Welcome!</Txt>
            <PlatformSpecificTxt>Welcome!</PlatformSpecificTxt>
        </View>
    );
};

export default StylingCustomTextExample;