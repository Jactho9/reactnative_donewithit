import {StyleSheet} from "react-native";
import {Colours} from "../../config/colours";

const Styles = StyleSheet.create({
    button: {
        backgroundColor: Colours.primary,
        borderRadius: 25,
        justifyContent: "center",
        alignItems: "center",
        padding: 15,
        width: "100%",
        marginVertical: 10
    },
    text: {
        color: Colours.white,
        fontSize: 18,
        textTransform: "uppercase",
        fontWeight: "bold",
    }
});

export default Styles;