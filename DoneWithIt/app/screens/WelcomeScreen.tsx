import React from 'react';
import {Image, ImageBackground, StyleSheet, Text, View} from "react-native";

import {Colours} from '../config/colours';
import Btn from "../components/Btn/Btn";
import {useNavigation} from "@react-navigation/native";

const WelcomeScreen = () => {

    const navigation = useNavigation();

    return (
        <ImageBackground
            blurRadius={2}
            style={styles.background}
            source={require('../assets/background.jpg')}
        >
            <View style={styles.logoContainer}>
                <Image style={styles.logo} source={require('../assets/logo-red.png')}/>
                <Text style={styles.tagLine}>Sell What You Don't Need</Text>
            </View>
            <View style={styles.buttonContainer}>
                <Btn text="Login" onPress={() => navigation.navigate("Login")}/>
                <Btn text="Register" colour={Colours.secondary} onPress={() => navigation.navigate("Register")}/>
            </View>
        </ImageBackground>
    );
};

const styles = StyleSheet.create({
    background: {
        flex: 1,
        justifyContent: "flex-end",
        alignItems: "center",
    },
    buttonContainer: {
        padding: 20,
        width: "100%"
    },
    logo: {
        width: 100,
        height: 100,
    },
    logoContainer: {
        alignItems: "center",
        position: "absolute",
        top: 70,
    },
    tagLine: {
        fontSize: 25,
        fontWeight: "600",
        paddingVertical: 20
    }

});

export default WelcomeScreen;