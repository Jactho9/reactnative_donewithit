import React from 'react';
import {useFormikContext} from "formik";

import ErrorMessage from "./ErrorMessage";
import ImageInputList from "../ImageInputList";

interface Props {
    name: string
}

const FormImagePicker = ({name} : Props) => {
    const { errors, setFieldValue, touched, values } = useFormikContext<{[key: string]: string[]}>();
    const imageUris = values[name];

    const handleAdd = (uri:string|undefined) => uri && setFieldValue(name, [...imageUris, uri]);
    const handleRemove = (uri:string|undefined) => uri && setFieldValue(name, imageUris.filter((imageUri:string) => imageUri !== uri));

    return (
        <>
            <ImageInputList
                /* @ts-ignore */
                imageUris={imageUris}
                onRemoveImage={handleRemove}
                onAddImage={handleAdd}/>
            <ErrorMessage error={errors[name]} visible={touched[name]}/>
        </>
    );
};

export default FormImagePicker;