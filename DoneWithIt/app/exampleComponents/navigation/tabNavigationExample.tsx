import React from 'react';
import Screen from "../../components/Screen";
import Txt from "../../components/Txt/Txt";
import {NavigationContainer, useNavigation} from "@react-navigation/native";
import Btn from "../../components/Btn/Btn";
import {createBottomTabNavigator} from "@react-navigation/bottom-tabs";
import TxtInput from "../../components/TxtInput/TxtInput";
import {MaterialCommunityIcons} from "@expo/vector-icons";

const Link = () => {
    const navigation = useNavigation();
    return <Btn text="Click" onPress={() => navigation.navigate("TweetDetails")}/>;
};

const Tweets = () => {
    const navigation = useNavigation();
    return (
        <Screen>
            <Txt>Tweets</Txt>
            <Btn text="View Tweet" onPress={() => navigation.navigate("TweetDetails", {id: 1})}/>
            <Link/>
        </Screen>
    )
};

const Account = () => <Screen><Txt>Account</Txt><TxtInput style={{backgroundColor: "#ccc"}}/></Screen>;

type RootTabParamList = {
    Feed: undefined;
    Account: { id: number };
};
const Tab = createBottomTabNavigator<RootTabParamList>();
const TabNavigator = () => (
    <Tab.Navigator
        tabBarOptions={{
            activeBackgroundColor: 'tomato',
            activeTintColor: 'white',
            inactiveBackgroundColor: '#eee',
            inactiveTintColor: 'black',
            labelStyle: {fontSize: 20}
        }}
    >
        <Tab.Screen
            options={{
                tabBarIcon: ({size, color}) => <MaterialCommunityIcons name="home" size={size} color={color}/>
            }}
            name="Feed"
            component={Tweets}
        />
        <Tab.Screen name="Account" component={Account}/>
    </Tab.Navigator>
);

const TabNavigationExample = () => {
    return (
        <NavigationContainer>
            <TabNavigator/>
        </NavigationContainer>
    );
};

export default TabNavigationExample;