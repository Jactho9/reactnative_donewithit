import React from 'react';
import {createStackNavigator} from "@react-navigation/stack";
import ListingsScreen, {IListing} from "../screens/ListingsScreen/ListingsScreen";
import ListingDetailsScreen from "../screens/ListingDetailsScreen/ListingDetailsScreen";
import {Routes} from "./routes";



export type FeedStackParamList = {
    Listings: undefined;
    ListingDetails: IListing;
};

const Stack = createStackNavigator<FeedStackParamList>();
const FeedNavigator = () => {
    return (
        <Stack.Navigator mode="modal" screenOptions={{headerShown: false}}>
            <Stack.Screen name={Routes.LISTINGS} component={ListingsScreen}/>
            <Stack.Screen name={Routes.LISTING_DETAILS} component={ListingDetailsScreen}/>
        </Stack.Navigator>
    );
};

export default FeedNavigator;