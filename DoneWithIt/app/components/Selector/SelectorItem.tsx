import React from 'react';
import {TouchableOpacity} from "react-native";
import Txt from "../Txt/Txt";
import {SelectorItemStyles as Styles} from "./selectorStyles";

interface Props{
    item: {
        label: string
        backgroundColour?: string
        icon?: string
        [key: string]: any;
    },
    onPress: () => void
}
export interface SelectorItemProps extends Props{}

const SelectorItem = ({ item, onPress }: Props) => {
    return (
        <TouchableOpacity onPress={onPress}>
            <Txt style={Styles.text}>{item.label}</Txt>
        </TouchableOpacity>
    );
};

export default SelectorItem;