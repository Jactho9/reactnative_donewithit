import {create} from 'apisauce';
import {AxiosRequestConfig} from "axios";
import {Cache} from "../utility/cache";
import {AuthStorage} from "../auth/storage";
import {Settings} from "../config/settings";

const apiClient = create({
    baseURL: Settings.apiUrl,
    timeout: 5000,
});

apiClient.addAsyncRequestTransform(async (request) => {
    const authToken = await AuthStorage.getToken();
    if(!authToken)
        return;
    request.headers["x-auth-token"] = authToken;
});


const get = apiClient.get;
// @ts-ignore
apiClient.get = async <T, U = T>(url: string, params?: {}, axiosConfig?: AxiosRequestConfig) => {

    const response = await get(url, params, axiosConfig);
    if(response.ok){
        Cache.store(url, response.data);
        return response;
    }

    const data = await Cache.get(url);
    return data ? { ok: true, data } : response;
};

export default apiClient;