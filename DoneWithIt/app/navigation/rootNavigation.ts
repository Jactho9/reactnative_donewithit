import React from "react";
import {NavigationContainerRef} from "@react-navigation/core";

export const navigationRef = React.createRef<NavigationContainerRef>();

const navigate = (name: string, params?: []) => {
    navigationRef.current?.navigate(name, params);
}

export const RootNavigation = {
    navigate,
}
