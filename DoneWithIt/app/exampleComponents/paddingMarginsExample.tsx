import React from 'react';
import {View} from "react-native";

const PaddingMarginsExample = () => {
    return (
        <View style={{
            flex: 1,
            alignItems: "center",
            justifyContent: "center",
        }}>
            <View style={{
                backgroundColor: "dodgerblue",
                width: 100,
                height: 100,
                padding: 10,
                paddingHorizontal: 25 //Overwrites normal padding value.
            }}>
                <View style={{
                    backgroundColor: "gold",
                    width: 50,
                    height: 50,
                }}/>
            </View>
            <View style={{
                backgroundColor: "tomato",
                width: 100,
                height: 100,
                marginTop: 20
            }}/>
        </View>
    );
};

export default PaddingMarginsExample;