import React, {useEffect} from 'react';
import {Alert, Image, StyleSheet, TouchableWithoutFeedback, View} from "react-native";
import {Colours} from "../config/colours";
import {MaterialCommunityIcons} from "@expo/vector-icons";
import * as ImagePicker from "expo-image-picker";
import {MediaTypeOptions} from "expo-image-picker";

interface Props {
    imageUri?: string,
    onChangeImage: (imageUri: string | undefined) => void
}

const ImageInput = ({imageUri, onChangeImage} : Props) => {

    const requestPermission = async () => {
        const { granted } = await ImagePicker.requestMediaLibraryPermissionsAsync();
        if(!granted)
            alert('You need to enable permission to access the library');
    };

    useEffect(() => {
        requestPermission();
    }, []);

    const handlePress = () => {
        if(!imageUri)
            selectImage();
        else
            Alert.alert("Delete", "Are you sure you want to delete this image?", [
                {text: "Yes", onPress: () => {onChangeImage(undefined)}},
                {text: "No"}
            ]);
    }

    const selectImage = async () => {
        try {
            const result = await ImagePicker.launchImageLibraryAsync({
                mediaTypes: MediaTypeOptions.Images,
                quality: 0.5
            });
            if(!result.cancelled)
                onChangeImage(result.uri);
        } catch {
            console.log("Error reading image");
        }
    }

    return (
        <TouchableWithoutFeedback onPress={handlePress}>
            <View style={styles.container}>
                {!imageUri && <MaterialCommunityIcons name="camera" size={40} color={Colours.medium}/>}
                {imageUri && <Image source={{uri: imageUri}} style={styles.image}/>}
            </View>
        </TouchableWithoutFeedback>
    );
};

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        backgroundColor: Colours.light,
        borderRadius: 15,
        height: 100,
        justifyContent: "center",
        width: 100,
        overflow: "hidden"
    },
    image: {
        width: "100%",
        height: "100%",
    }
});

export default ImageInput;