import React from 'react';
import {View} from "react-native";
import NetInfo, {useNetInfo} from '@react-native-community/netinfo';
import Txt from "../components/Txt/Txt";
import Btn from "../components/Btn/Btn";

const NetInfoExample = () => {

    //NetInfo.fetch().then(netInfo => console.log(netInfo));
    /*const unsubscribe = NetInfo.addEventListener(netInfo => console.log(netInfo));
    unsubscribe();*/

    const netInfo = useNetInfo();

    if(netInfo.isInternetReachable)
        return <Txt>Reachable</Txt>

    return <Txt>Not Reachable</Txt>;
};

export default NetInfoExample;