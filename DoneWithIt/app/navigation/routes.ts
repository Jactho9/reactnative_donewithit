export const Routes = {
    LISTINGS: "Listings",
    LISTING_DETAILS: "ListingDetails",
    LISTINGS_EDIT: "ListingsEdit",
} as const;