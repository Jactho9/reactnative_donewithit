import React from "react";
import { Alert, Keyboard } from "react-native";
import * as Yup from "yup";
import * as Notifications from "expo-notifications";

import { AppForm, FormField, SubmitBtn } from "./forms";
import {MessagesApi} from "../api/messages";
import {IListing} from "../screens/ListingsScreen/ListingsScreen";
import {FormikHelpers} from "formik/dist/types";
import {IFormListing} from "../api/listings";

const validationSchema = Yup.object().shape({
    message: Yup.string().required().min(1).label("Message"),
});

interface IContactSellerFormProps {
    listing: IListing
}

const ContactSellerForm = ({ listing }: IContactSellerFormProps) => {
    const handleSubmit = async ({ message }: {message: string}, { resetForm }: FormikHelpers<IFormListing>) => {
        Keyboard.dismiss();

        const result = await MessagesApi.send(message, listing.id);

        if (!result.ok) {
            console.log("Error", result);
            return Alert.alert("Error", "Could not send the message to the seller.");
        }

        resetForm();

        Notifications.scheduleNotificationAsync({
            content: {
                title: "Awesome!",
                body: "Your message was sent to the seller.",
            },
            trigger: null
        });
    };

    return (
        <AppForm
            initialValues={{ message: "" }}
            onSubmit={handleSubmit}
            validationSchema={validationSchema}
        >
            <FormField
                maxLength={255}
                multiline
                name="message"
                numberOfLines={3}
                placeholder="Message..."
            />
            <SubmitBtn text="Contact Seller" />
        </AppForm>
    );
}

export default ContactSellerForm;
