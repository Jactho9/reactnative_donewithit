import React from 'react';
import {View, TouchableOpacity} from "react-native";
import Styles from "./styles";
import Txt from "../Txt/Txt";
import {MaterialCommunityIcons} from "@expo/vector-icons";

const NavigationBar = () => {
    return (
        <View style={Styles.container}>
            <TouchableOpacity>
                <MaterialCommunityIcons name="email" color="blue" size={30}/>
                <Txt numberOfLines={1}>Navigator</Txt>
            </TouchableOpacity>
        </View>
    );
};

export default NavigationBar;