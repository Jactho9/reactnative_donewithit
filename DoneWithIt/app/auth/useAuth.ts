import {useContext} from "react";
import {AuthContext} from "./context";
import {AuthStorage} from "./storage";
import jwtDecode from "jwt-decode";
import {IAuthUser} from "../api/auth";

export const useAuth = () => {
    const {user, setUser} = useContext(AuthContext)!;

    const login = (authToken: string) => {
        const user = jwtDecode(authToken) as IAuthUser;
        setUser(user);
        AuthStorage.storeToken(authToken);
    }

    const logout = () => {
        setUser(undefined);
        AuthStorage.removeToken();
    }

    return {login, logout, user};
}
