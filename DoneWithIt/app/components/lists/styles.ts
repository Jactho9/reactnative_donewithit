import {StyleSheet} from "react-native";
import {Colours} from "../../config/colours";

export const ListItemStyles = StyleSheet.create({
    container:{
        flexDirection: "row",
        padding: 15,
        alignItems: "center",
        backgroundColor: Colours.white
    },
    image: {
        width: 70,
        height: 70,
        borderRadius: 35,
    },
    title: {
        fontWeight: "500"
    },
    subTitle: {
        color: Colours.medium
    },
    detailsContainer: {
        marginLeft: 10,
        justifyContent: "center",
        flex:1
    }
});


export const SeparatorStyles = StyleSheet.create({
    separator:{
        width: '100%',
        height: 1,
        backgroundColor: Colours.light
    }
});


export const DeleteActionStyles = StyleSheet.create({
    container:{
        backgroundColor: Colours.danger,
        width: 70,
        justifyContent: "center",
        alignItems: "center",
    }
});
