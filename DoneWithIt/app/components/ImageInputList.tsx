import React, { useRef } from 'react';
import { ScrollView, StyleSheet, View} from "react-native";
import ImageInput from "./ImageInput";

interface Props {
    imageUris?: string[],
    onRemoveImage: (imageUri: string | undefined) => void
    onAddImage: (imageUri: string | undefined) => void
}

const ImageInputList = ({imageUris = [], onRemoveImage, onAddImage} : Props) => {

    const scrollView = useRef<ScrollView|null>(null);

    return (
        <View>
            <ScrollView ref={scrollView} horizontal onContentSizeChange={() => scrollView.current?.scrollToEnd()}>
                <View style={styles.container}>
                    {imageUris.map(uri =>
                        <View key={uri} style={styles.image}>
                            <ImageInput imageUri={uri} onChangeImage={() => onRemoveImage(uri)}/>
                        </View>
                    )}
                    <ImageInput onChangeImage={uri => onAddImage(uri)}/>
                </View>
            </ScrollView>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flexDirection: "row"
    },
    image: {
        marginRight: 10
    }
});

export default ImageInputList;