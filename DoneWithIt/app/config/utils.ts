import {Platform} from "react-native";

export const Utils = {
    isAndroid: Platform.OS === "android",
    isIos: Platform.OS === "ios"
};