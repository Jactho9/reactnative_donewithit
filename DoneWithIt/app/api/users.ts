import {IAuthCredentials} from "./auth";
import apiClient from "./client";

export interface IUser extends IAuthCredentials{
    name: string
    id?: number
    expoPushToken?: string
}

const register = (userInfo: IUser) => apiClient.post<IUser>("/users", userInfo);

export const UsersApi = {
    register,
}