import React from 'react';
import {View} from "react-native";

const FlexExample = () => {
    return (
        <View style={{
            backgroundColor: "#ccc",
            flex: 1,
            flexDirection: "row",
            justifyContent: "space-between",
            flexWrap: "wrap",
            alignContent: "center"
        }}>
            <View style={{
                backgroundColor: "dodgerblue",
                width: 100,
                height: 100,
            }}/>
            <View style={{
                backgroundColor: "gold",
                width: 100,
                height: 100
            }}/>
            <View style={{
                backgroundColor: "tomato",
                width: 100,
                height: 100
            }}/>
            <View style={{
                backgroundColor: "green",
                width: 100,
                height: 100
            }}/>
            <View style={{
                backgroundColor: "purple",
                width: 100,
                height: 100
            }}/>
        </View>
    );
};

export default FlexExample;