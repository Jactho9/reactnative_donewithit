import React from 'react';
import {createBottomTabNavigator} from "@react-navigation/bottom-tabs";
import {MaterialCommunityIcons} from "@expo/vector-icons";

import NewListingBtn from "./NewListingBtn";
import {Routes} from "./routes";
import ListingEditScreen from "../screens/ListingEditScreen";
import FeedNavigator from "./FeedNavigator";
import AccountNavigator from "./AccountNavigator";
import {useNotifications} from "../hooks/useNotifications";
import {RootNavigation} from "./rootNavigation";


const Tab = createBottomTabNavigator();
const AppNavigator = () => {

    useNotifications(notification => {
        RootNavigation.navigate("Account");
    });

    return (
        <Tab.Navigator>
            <Tab.Screen name="Feed" component={FeedNavigator} options={
                {tabBarIcon: ({color, size}) => <MaterialCommunityIcons name="home" color={color} size={size}/>}
            }/>
            <Tab.Screen name={Routes.LISTINGS_EDIT} component={ListingEditScreen} options={
                ({navigation}) => ({tabBarButton: () => <NewListingBtn onPress={() => navigation.navigate("ListingsEdit")}/>})
            }/>
            <Tab.Screen name="Account" component={AccountNavigator} options={
                {tabBarIcon: ({color, size}) => <MaterialCommunityIcons name="account" color={color} size={size}/>}
            }/>
        </Tab.Navigator>
    );
};

export default AppNavigator;