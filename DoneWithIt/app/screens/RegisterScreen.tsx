import React, {useState} from 'react';
import { StyleSheet } from "react-native";
import {AppForm, FormField, SubmitBtn} from "../components/forms";
import Screen from "../components/Screen";
import * as Yup from "yup";
import {IUser, UsersApi} from "../api/users";
import {useAuth} from "../auth/useAuth";
import {Auth} from "../api/auth";
import {useApi} from "../hooks/useApi";
import ActivityIndicator from "../components/ActivityIndicator";

const validationSchema = Yup.object().shape({
    name: Yup.string().required().label("Name"),
    email: Yup.string().required().email().label("Email"),
    password: Yup.string().required().min(4).label("Password"),
});

const RegisterScreen = () => {
    const registerApi = useApi(UsersApi.register);
    const loginApi = useApi(Auth.login);
    const auth = useAuth();
    const [error, setError] = useState<string>();

    const handleSubmit = async (userInfo: any) => {
        const result = await registerApi.request(userInfo);
        if(!result.ok){
            if(result.data)
            {
                // @ts-ignore
                setError(result.data.error);
            }
            else {
                setError("An unexpected error occurred");
                console.log(result);
            }
            return;
        }

        const {data: authToken} = await loginApi.request(userInfo.email, userInfo.password);
        auth.login(authToken as string);

    }

    return (
        <>
            <ActivityIndicator visible={registerApi.loading || loginApi.loading}/>
            <Screen style={styles.container}>
                <AppForm
                    initialValues={{ name: "", email: "", password: "" }}
                    onSubmit={values => handleSubmit(values)}
                    validationSchema={validationSchema}
                >
                    <FormField
                        autoCorrect={false}
                        icon="account"
                        name="name"
                        placeholder="Name"
                    />
                    <FormField
                        autoCapitalize="none"
                        autoCorrect={false}
                        icon="email"
                        keyboardType="email-address"
                        name="email"
                        textContentType="emailAddress"
                        placeholder="Email"
                    />
                    <FormField
                        autoCorrect={false}
                        icon="lock"
                        name="password"
                        placeholder="Password"
                        secureTextEntry
                        textContentType="password"
                        autoCapitalize="none"
                    />
                    <SubmitBtn text="Register"/>
                </AppForm>
            </Screen>
        </>
    );
};

const styles = StyleSheet.create({
    container: {
        padding: 10,
    },
});

export default RegisterScreen;