import React from 'react';
import {View} from "react-native";
import {MaterialCommunityIcons} from '@expo/vector-icons';

const IconsExample = () => {
    return (
        <View style={{
            flex: 1,
            alignItems: "center",
            justifyContent: "center",
        }}>
            <MaterialCommunityIcons name="email" size={200} color="dodgerblue"/>
        </View>
    );
};

export default IconsExample;