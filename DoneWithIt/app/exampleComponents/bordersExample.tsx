import React from 'react';
import {View} from "react-native";

const BordersExample = () => {
    return (
        <View style={{
            flex: 1,
            alignItems: "center",
            justifyContent: "center",
        }}>
            <View style={{
                backgroundColor: "dodgerblue",
                width: 100,
                height: 100,
                borderWidth: 10,
                borderColor: "royalblue",
                borderRadius: 10,
                borderTopWidth: 20,
            }}/>
            <View style={{
                backgroundColor: "dodgerblue",
                width: 100,
                height: 100,
                borderWidth: 10,
                borderColor: "royalblue",
                borderRadius: 50,
            }}/>
        </View>
    );
};

export default BordersExample;