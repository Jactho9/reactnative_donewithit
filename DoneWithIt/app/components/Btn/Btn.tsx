import React from 'react';
import {Text, TouchableOpacity} from "react-native";
import Styles from "./styles";
import {Colours} from "../../config/colours";

interface Props {
    text: string
    colour?: string
    onPress?: () => void
}

const Btn = ({text, onPress, colour = Colours.primary} : Props) => {
    return (
        <TouchableOpacity style={[Styles.button, {backgroundColor: colour}]} onPress={onPress}>
            <Text style={Styles.text}>{text}</Text>
        </TouchableOpacity>
    );
};

export default Btn;