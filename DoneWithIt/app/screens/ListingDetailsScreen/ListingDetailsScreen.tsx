import React from 'react';
import {KeyboardAvoidingView, Platform, View} from "react-native";
import Txt from "../../components/Txt/Txt";
import Styles from "./styles";
import ListItem from "../../components/lists/ListItem";
import {RouteProp, useRoute} from "@react-navigation/native";
import {FeedStackParamList} from "../../navigation/FeedNavigator";
import {Image} from "react-native-expo-image-cache";
import ContactSellerForm from "../../components/ContactSellerForm";

const ListingDetailsScreen = () => {
    const {params : listing} = useRoute<RouteProp<FeedStackParamList, "ListingDetails">>();

    return (
        <KeyboardAvoidingView behavior="position" keyboardVerticalOffset={Platform.OS === "ios" ? 0 : 100}>
            <View style={Styles.card}>
                <Image uri={listing.images[0].url} preview={{uri: listing.images[0].thumbnailUrl}} tint="light" style={Styles.image}/>
                <View style={Styles.detailsContainer}>
                    <Txt style={Styles.title}>{listing.title}</Txt>
                    <Txt style={Styles.subTitle}>{listing.price}</Txt>
                    <View style={Styles.userContainer}>
                        <ListItem image={require("../../assets/mosh.jpg")} title="Mosh Hamedani" subTitle="5 listings"/>
                    </View>
                </View>
                <ContactSellerForm listing={listing}/>
            </View>
        </KeyboardAvoidingView>
    );
};

export default ListingDetailsScreen;