import React from "react";
import {IAuthUser} from "../api/auth";

export interface IAuthContext {
    user?: IAuthUser
    setUser: React.Dispatch<React.SetStateAction<IAuthUser | undefined>>
}

export const AuthContext = React.createContext<IAuthContext | null>(null);
