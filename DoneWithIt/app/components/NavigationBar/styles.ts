import {StyleSheet} from "react-native";
import {Colours} from "../../config/colours";

const Styles = StyleSheet.create({
    container:{
        flexDirection: "row",
        backgroundColor: Colours.medium,
    }
});

export default Styles;