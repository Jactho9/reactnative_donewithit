import React from 'react';
import {StyleSheet, Button} from "react-native";
import Screen from "../components/Screen";
import * as Notifications from "expo-notifications";



const LocalNotificationExample = () => {

    const showNotification = () => {
        const content = { title: 'Congratulations', body: "You order was successfully placed!" };

        Notifications.scheduleNotificationAsync({ content, trigger: new Date().getTime() + 4000 }); //Trigger should be null for immediate notification
    }

    return (
        <Screen>
            <Button title={"Tap Me"} onPress={showNotification}/>
        </Screen>
    );
};

const styles = StyleSheet.create({
    container: {}
});

export default LocalNotificationExample;