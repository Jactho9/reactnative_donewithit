import React, {useState} from 'react';
import {FlatList, ImageSourcePropType} from "react-native";

import ListItem from "../components/lists/ListItem";
import Screen from "../components/Screen";
import ListItemSeparator from "../components/lists/ListItemSeparator";
import ListItemDeleteAction from "../components/lists/ListItemDeleteAction";

interface Message {
    id: number,
    title: string,
    description: string,
    image: ImageSourcePropType
}

const initialMessages : Message[] = [
    {id: 1, title: 'T1', description: 'D1', image: require('../assets/mosh.jpg')},
    {id: 2, title: 'T2', description: 'D2', image: require('../assets/mosh.jpg')},
    {id: 3, title: 'T3', description: 'D3', image: require('../assets/mosh.jpg')},
    {id: 4, title: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vulputate sapien in lorem pharetra bibendum. Etiam nec tellus eget risus lobortis lobortis. Morbi porttitor nulla id mauris blandit iaculis. In eros enim, congue eu sollicitudin vel, blandit lobortis eros. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent urna diam, tincidunt non neque ac, varius eleifend ex.', description: 'Ut venenatis arcu lorem, mollis luctus nulla consectetur in. Duis euismod massa eu elit vulputate, id aliquet eros efficitur. Phasellus et finibus ligula. Duis volutpat auctor consequat. Donec vulputate velit arcu, a auctor leo pharetra eu. Donec porta vel nisl vel porttitor. Suspendisse quis tempus mi, eu consectetur arcu. Duis finibus, arcu a ultrices pellentesque, libero nisi congue nulla, a volutpat arcu augue nec elit. Curabitur volutpat tincidunt nisi, ut vehicula quam hendrerit et. Vestibulum purus ipsum, tincidunt eget dapibus ut, varius nec turpis.', image: require('../assets/mosh.jpg')}
];

const MessagesScreen = () => {
    const [messages, setMessages] = useState(initialMessages);
    const [refreshing, setRefreshing] = useState(false);

    const handleDelete = (message: Message) => {
        setMessages(messages.filter(m => m.id !== message.id));
    };

    return (
        <Screen>
            <FlatList
                data={messages}
                keyExtractor={m => m.id.toString()}
                renderItem={({item}) =>
                    <ListItem
                        title={item.title}
                        subTitle={item.description}
                        image={item.image}
                        onPress={() => console.log("Message selected", item)}
                        renderRightActions={() =>
                            <ListItemDeleteAction onPress={() => handleDelete(item)}/>
                        }
                    />
                }
                ItemSeparatorComponent={ListItemSeparator}
                refreshing={refreshing}
                onRefresh={() => {
                    setMessages(initialMessages);
                }}
            />
        </Screen>
    );
};

export default MessagesScreen;