import React from 'react';
import {Props as TxtInputProps} from "../TxtInput/TxtInput";
import { useFormikContext } from 'formik';
import ErrorMessage from "./ErrorMessage";
import Selector, {SelectorItemDetails} from "../Selector/Selector";
import {SelectorItemProps} from "../Selector/SelectorItem";

interface Props extends TxtInputProps{
    name: string,
    items: SelectorItemDetails[],
    numberOfColumns?: number
    placeholder?: string,
    SelectorItemComponent?: React.FC<SelectorItemProps>,
    width: string | number
}

const FormSelector = ({ items, name, placeholder, numberOfColumns, SelectorItemComponent, width} : Props) => {

    const { errors, setFieldValue, touched, values } = useFormikContext();

    return (
        <>
            <Selector
                onSelectItem={(item) => setFieldValue(name, item)}
                placeholder={placeholder}
                numberOfColumns={numberOfColumns}
                /* @ts-ignore */
                selectedItem={values[name]}
                SelectorItemComponent={SelectorItemComponent}
                items={items}
                width={width}
            />
            {/* @ts-ignore */}
            <ErrorMessage error={errors[name]} visible={touched[name]}/>
        </>
    );
};



export default FormSelector;