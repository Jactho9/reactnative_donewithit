import client from "./client";

export interface IAuthCredentials {
    email: string
    password: string
}

export interface IAuthUser {
    email: string,
    iat: number,
    name: string,
    userId: number,
}

const login = (email: string, password: string) => client.post('/auth', {email, password});

export const Auth = {
    login,
};
