import client from "./client";

const send = (message: string, listingId: number | string) =>
    client.post("/messages", {
        message,
        listingId,
    });

export const MessagesApi = {
    send,
};
