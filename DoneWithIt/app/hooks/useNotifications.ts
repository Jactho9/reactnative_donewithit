import {useEffect} from "react";
import {ExpoPushTokensApi} from "../api/expoPushTokens";
import * as Notifications from "expo-notifications";
import * as Permissions from "expo-permissions";

export const useNotifications = (notificationResponseReceivedListener: (event: Notifications.NotificationResponse) => void) => {
    useEffect(()=>{

        registerForPushNotifications();

        if(notificationResponseReceivedListener)
            Notifications.addNotificationResponseReceivedListener(notificationResponseReceivedListener);

    }, []);

    const registerForPushNotifications = async () => {
        try{
            const permission = await Permissions.askAsync(Permissions.NOTIFICATIONS);
            if(!permission.granted)
                return;

            const token = await Notifications.getExpoPushTokenAsync();
            ExpoPushTokensApi.register(token.data);
        } catch (e) {
            console.log("Error getting a push token", e);
        }
    }
}
