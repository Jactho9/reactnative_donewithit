import React from 'react';
import {View, StyleSheet} from "react-native";
import Txt from "./Txt/Txt";
import Constants from 'expo-constants';
import {useNetInfo} from "@react-native-community/netinfo";
import {Colours} from "../config/colours";

const OfflineNotice = () => {

    const netInfo = useNetInfo();
    if(netInfo.type === "unknown" || netInfo.isInternetReachable !== false)
        return null;

    return (
        <View style={styles.container}>
            <Txt style={styles.text}>No Internet Connection</Txt>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colours.primary,
        height: 50,
        position: "absolute",
        zIndex: 1,
        width: "100%",
        top: Constants.statusBarHeight,
        alignItems: "center",
        justifyContent: "center",
    },
    text: {
        color: Colours.white,
    }
});

export default OfflineNotice;