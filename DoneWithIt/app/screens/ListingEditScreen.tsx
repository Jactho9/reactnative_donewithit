import React, {useState} from 'react';
import { StyleSheet } from "react-native";
import {AppForm, FormField, SubmitBtn} from "../components/forms";
import * as Yup from "yup";

import Screen from "../components/Screen";
import FormSelector from "../components/forms/FormSelector";
import SelectorItemCategory from "../components/Selector/SelectorItemCategory";
import FormImagePicker from "../components/forms/FormImagePicker";
import useLocation from "../hooks/useLocation";
import {IFormListing, IListingApi, ListingsApi} from "../api/listings";
import UploadScreen from "./UploadScreen";
import {FormikBag} from "formik";
import {FormikHelpers} from "formik/dist/types";

const validationSchema = Yup.object().shape({
    title: Yup.string().required().min(1).label("Title"),
    price: Yup.number().required().min(1).max(10000).label("Price"),
    description: Yup.string().label("Description"),
    category: Yup.object().required().nullable().label("Category"),
    images: Yup.array().min(1, "Please select at least one image"),
});

const categories = [
    {
        backgroundColour: "#fc5c65",
        icon: "floor-lamp",
        label: "Furniture",
        value: 1,
    },
    {
        backgroundColour: "#fd9644",
        icon: "car",
        label: "Cars",
        value: 2,
    },
    {
        backgroundColour: "#fed330",
        icon: "camera",
        label: "Cameras",
        value: 3,
    },
    {
        backgroundColour: "#26de81",
        icon: "cards",
        label: "Games",
        value: 4,
    },
    {
        backgroundColour: "#2bcbba",
        icon: "shoe-heel",
        label: "Clothing",
        value: 5,
    },
    {
        backgroundColour: "#45aaf2",
        icon: "basketball",
        label: "Sports",
        value: 6,
    },
    {
        backgroundColour: "#4b7bec",
        icon: "headphones",
        label: "Movies & Music",
        value: 7,
    },
    {
        backgroundColour: "#a55eea",
        icon: "book-open-variant",
        label: "Books",
        value: 8,
    },
    {
        backgroundColour: "#778ca3",
        icon: "application",
        label: "Other",
        value: 9,
    },
];


const ListingEditScreen = () => {

    const location = useLocation();
    const [uploadVisible, setUploadVisible] = useState(false);
    const [progress, setProgress] = useState(0);

    const handleSubmit = async (listing: IFormListing, {resetForm}: FormikHelpers<IFormListing>) => {
        setProgress(0);
        setUploadVisible(true);
        const result = await ListingsApi.addListing(
            { ...listing, location },
            progress => setProgress(progress)
        );

        if(!result.ok) {
            setUploadVisible(false);
            return alert("Could not save the listing. " + result.problem + " Axios Error: " + result.originalError.message);
        }

        resetForm();
    }

    return (
        <Screen style={styles.container}>
            <UploadScreen onDone={() => setUploadVisible(false)} progress={progress} visible={uploadVisible}/>
            <AppForm
                initialValues={{
                    title: "",
                    price: "",
                    description: "",
                    category: null,
                    images: [],
                }}
                onSubmit={handleSubmit}
                validationSchema={validationSchema}
            >
                <FormImagePicker name="images"/>
                <FormField maxLength={255} name="title" placeholder="Title" />
                <FormField
                    keyboardType="numeric"
                    maxLength={8}
                    name="price"
                    placeholder="Price"
                    width={120}
                />
                <FormSelector
                    name={"category"}
                    items={categories}
                    numberOfColumns={3}
                    placeholder="Category"
                    SelectorItemComponent={SelectorItemCategory}
                    width="50%"
                />
                <FormField
                    maxLength={255}
                    multiline
                    name="description"
                    numberOfLines={3}
                    placeholder="Description"
                />
                <SubmitBtn text="Post"/>
            </AppForm>
        </Screen>
    );
};

const styles = StyleSheet.create({
    container: {
        padding: 10,
    },
});

export default ListingEditScreen;