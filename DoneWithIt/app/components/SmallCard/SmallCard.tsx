import React from 'react';
import {TouchableOpacity} from "react-native";
import Styles from "./styles";
import Txt from "../Txt/Txt";
import {MaterialCommunityIcons} from "@expo/vector-icons";

interface Props {
    title: string
    icon: any,
    style?: any
}

const SmallCard = ({title, icon, style} : Props) => {
    return (
        <TouchableOpacity style={[Styles.smallCard, style]}>
            <MaterialCommunityIcons name={icon} color="blue" size={30}/>
            <Txt style={Styles.title} numberOfLines={1}>{title}</Txt>
        </TouchableOpacity>
    );
};

export default SmallCard;