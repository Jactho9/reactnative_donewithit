import React from 'react';
import {StyleSheet} from "react-native";
import Txt from "../Txt/Txt";
import {Colours} from "../../config/colours";

interface Props {
    error: string | string[] | undefined,
    visible?: boolean | undefined
}

const ErrorMessage = ({error, visible} : Props) => {
    if(!visible || !error)
        return null;

    return (
        <Txt style={styles.text}>{error}</Txt>
    );
};

const styles = StyleSheet.create({
    text: {
        color: Colours.danger
    }
});

export default ErrorMessage;