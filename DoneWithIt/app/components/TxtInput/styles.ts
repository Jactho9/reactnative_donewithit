import {Platform, StyleSheet} from "react-native";
import {Colours} from "../../config/colours";

const Styles = StyleSheet.create({
    container:{
        flexDirection: "row",
        alignItems: "center",
        backgroundColor: Colours.light,
        borderRadius: 25,
        padding: 15,
        marginVertical: 10,
    },
    textInput: {
        fontSize: 18,
        fontFamily: Platform.OS === "android" ? "Roboto" : "Avenir",
        color: Colours.dark
    },
    icon: {
        marginRight: 10
    }
});

export default Styles;