import React from 'react';
import {View} from "react-native";
import Btn from "../components/Btn/Btn";

const CustomButtonExample = () => {
    return (
        <View style={{
            flex: 1,
            alignItems: "center",
            justifyContent: "center",
        }}>
            <Btn text={"Hello there"} onPress={() => console.log("Tapped")}/>
        </View>
    );
};

export default CustomButtonExample;