import {Platform} from "react-native";
import {Colours} from "./colours";

export const DefaultStyles = {
    text: {
        fontSize: 18,
        fontFamily: Platform.OS === "android" ? "Roboto" : "Avenir",
        color: Colours.dark,
        /*flex:1*/
    }
}