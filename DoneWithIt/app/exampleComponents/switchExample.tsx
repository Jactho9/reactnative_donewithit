import React, {useState} from 'react';
import Screen from "../components/Screen";
import {Switch} from "react-native";

const SwitchExample = () => {
    const [isNew, setIsNew] = useState(false);
    return (
        <Screen>
            <Switch value={isNew} onValueChange={newValue => setIsNew(newValue)}/>
        </Screen>
    );
};

export default SwitchExample;