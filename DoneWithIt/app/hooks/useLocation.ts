import {useEffect, useState} from "react";
import * as Location from "expo-location";

const useLocation = () => {
    const [location, setLocation] = useState<{latitude:number, longitude:number}>()

    const getLocation = async () => {
        try{
            const {granted} = await Location.requestPermissionsAsync();
            // const {granted} = await Location.requestForegroundPermissionsAsync();
            if(!granted)
                return;
            const location = await Location.getLastKnownPositionAsync();
            if(!location)
                return;
            const {coords: {latitude, longitude}} = location;
            setLocation({latitude, longitude});
        } catch (error) {
            console.log(error)
        }
    }

    useEffect(() => {
        getLocation();
    }, []);

    return location;
};

export default useLocation;