import {StyleSheet} from "react-native";
import {Colours} from "../../config/colours";

const Styles = StyleSheet.create({
    card:{
        borderRadius: 15,
        backgroundColor: Colours.white,
        marginBottom: 20,
        overflow: "hidden"
    },
    detailsContainer:{
        padding: 20,
    },
    image:{
        width: "100%",
        height: 200,
    },
    subTitle: {
        color: Colours.secondary,
        fontWeight: "bold"
    },
    title: {
        marginBottom: 7
    },
});

export default Styles;