import React from 'react';
import {Text, View} from "react-native";

const StylingTextExample = () => {
    return (
        <View style={{
            flex: 1,
            alignItems: "center",
            justifyContent: "center",
        }}>
            <Text style={{
                fontSize: 30,
                fontStyle: "italic",
                fontWeight: "600",
                color: "tomato",
                textTransform: "capitalize"
            }}>This is some Text!</Text>
            <Text style={{
                padding: 30,
                fontSize: 30,
                color: "green",
                textAlign: "center",
                lineHeight: 50
            }}>This is a long line of text. It is going to be a lot longer than the previous one so that it shows more text.</Text>
        </View>
    );
};

export default StylingTextExample;