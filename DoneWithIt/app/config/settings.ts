import Constants from "expo-constants";

const baseURL_Laptop = "http://172.20.10.2:9000/api/";
const baseURL_Desktop = "http://10.1.1.148:9000/api/";


const allSettings = {
    dev: {
        apiUrl: baseURL_Desktop,
    },
    staging: {
        apiUrl: baseURL_Desktop,
    },
    prod: {
        apiUrl: baseURL_Desktop,
    }
}

const getCurrentSettings = () => {
    if(__DEV__)
        return allSettings.dev;
    if(Constants.manifest.releaseChannel === "staging")
        return allSettings.staging;
    return allSettings.prod;
}

export const Settings = getCurrentSettings();