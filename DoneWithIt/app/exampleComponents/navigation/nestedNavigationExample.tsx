import React from 'react';
import Screen from "../../components/Screen";
import Txt from "../../components/Txt/Txt";
import {createStackNavigator} from "@react-navigation/stack";
import {NavigationContainer, RouteProp, useNavigation, useRoute} from "@react-navigation/native";
import Btn from "../../components/Btn/Btn";
import {createBottomTabNavigator} from "@react-navigation/bottom-tabs";
import TxtInput from "../../components/TxtInput/TxtInput";
import {MaterialCommunityIcons} from "@expo/vector-icons";

const Link = () => {
    const navigation = useNavigation();
    return <Btn text="Click" onPress={() => navigation.navigate("TweetDetails")}/>;
};

const Tweets = () => {
    const navigation = useNavigation();
    return (
        <Screen>
            <Txt>Tweets</Txt>
            <Btn text="View Tweet" onPress={() => navigation.navigate("TweetDetails", {id: 1})}/>
            <Link/>
        </Screen>
    )
};

const TweetDetails = () => {
    const route = useRoute<RouteProp<RootStackParamList, 'TweetDetails'>>();
    return (
        <Screen>
            <Txt>TweetDetails {route?.params?.id}</Txt>
        </Screen>
    );
};

type RootStackParamList = {
    Tweets: undefined;
    TweetDetails: { id: number };
};
const Stack = createStackNavigator<RootStackParamList>();
const FeedNavigator = () => (
    <Stack.Navigator screenOptions={{
        headerStyle: {
            backgroundColor: "dodgerblue",
        },
        headerTintColor: "white",
    }}>
        <Stack.Screen
            name="Tweets"
            component={Tweets}
            options={{
                headerStyle: {
                    backgroundColor: "tomato",
                },
                headerTintColor: "white",
                headerShown: true
            }}
        />
        <Stack.Screen
            name="TweetDetails"
            component={TweetDetails}
            options={({route}) => ({title: "Tweet Details " + route?.params?.id})}
        />
    </Stack.Navigator>
);


const Account = () => <Screen><Txt>Account</Txt><TxtInput style={{backgroundColor: "#ccc"}}/></Screen>;

type RootTabParamList = {
    Feed: undefined;
    Account: { id: number };
};
const Tab = createBottomTabNavigator<RootTabParamList>();
const TabNavigator = () => (
    <Tab.Navigator
        tabBarOptions={{
            activeBackgroundColor: 'tomato',
            activeTintColor: 'white',
            inactiveBackgroundColor: '#eee',
            inactiveTintColor: 'black',
            labelStyle: {fontSize: 20}
        }}
    >
        <Tab.Screen
            options={{
                tabBarIcon: ({size, color}) => <MaterialCommunityIcons name="home" size={size} color={color}/>
            }}
            name="Feed"
            component={FeedNavigator}
        />
        <Tab.Screen name="Account" component={Account}/>
    </Tab.Navigator>
);

const NestedNavigationExample = () => {
    return (
        <NavigationContainer>
            <TabNavigator/>
        </NavigationContainer>
    );
};

export default NestedNavigationExample;