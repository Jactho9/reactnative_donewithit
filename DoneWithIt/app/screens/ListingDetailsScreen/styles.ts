import {StyleSheet} from "react-native";
import {Colours} from "../../config/colours";

const Styles = StyleSheet.create({
    card:{
        borderRadius: 15,
        backgroundColor: Colours.white,
        marginBottom: 20,
        overflow: "hidden"
    },
    detailsContainer:{
        padding: 20,
    },
    image:{
        width: "100%",
        height: 300,
    },
    subTitle: {
        color: Colours.secondary,
        fontWeight: "bold",
        fontSize: 20,
        marginVertical: 10
    },
    title: {
        fontSize: 24,
        fontWeight: "500",
    },
    userContainer: {
        marginVertical: 40,
    }

});

export default Styles;