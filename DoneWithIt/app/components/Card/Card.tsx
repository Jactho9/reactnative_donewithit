import React from 'react';
import {View, TouchableWithoutFeedback} from "react-native";
import Styles from "./styles";
import Txt from "../Txt/Txt";
import {Image} from "react-native-expo-image-cache";

interface Props {
    title: string
    subTitle?: string
    imageUrl: string,
    thumbnailUrl: string,
    onPress?: () => void
}

const Card = ({title, subTitle, imageUrl, onPress, thumbnailUrl} : Props) => {
    return (
        <TouchableWithoutFeedback onPress={onPress}>
            <View style={Styles.card}>
                <Image uri={imageUrl} style={Styles.image} preview={{uri: thumbnailUrl}} tint={"light"}/>
                <View style={Styles.detailsContainer}>
                    <Txt style={Styles.title} numberOfLines={1}>{title}</Txt>
                    <Txt style={Styles.subTitle} numberOfLines={1}>{subTitle}</Txt>
                </View>
            </View>
        </TouchableWithoutFeedback>
    );
};

export default Card;