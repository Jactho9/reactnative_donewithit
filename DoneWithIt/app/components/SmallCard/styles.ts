import {StyleSheet} from "react-native";
import {Colours} from "../../config/colours";

const Styles = StyleSheet.create({
    smallCard:{
        borderRadius: 5,
        backgroundColor: Colours.white,
        marginBottom: 20,
        overflow: "hidden",
        borderWidth: 1,
        height: 70,
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        elevation: 2,
    },
    title: {
        color: "blue",
        fontSize: 20,
        fontWeight: "bold"
    },



    card:{
        borderRadius: 15,
        backgroundColor: Colours.white,
        marginBottom: 20,
        overflow: "hidden"
    },
    detailsContainer:{
        padding: 20,
    },
    image:{
        width: "100%",
        height: 200,
    },
    subTitle: {
        color: Colours.secondary,
        fontWeight: "bold"
    },
});

export default Styles;