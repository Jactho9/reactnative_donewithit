import React from 'react';
import {TouchableWithoutFeedback, View} from "react-native";
import {DeleteActionStyles} from "./styles";
import {MaterialCommunityIcons} from "@expo/vector-icons";
import {Colours} from "../../config/colours";

interface Props{
    onPress: () => void
}

const ListItemDeleteAction = ({onPress} : Props) => {
    return (
        <TouchableWithoutFeedback onPress={onPress}>
            <View style={DeleteActionStyles.container}>
                <MaterialCommunityIcons name="trash-can" size={35} color={Colours.white}/>
            </View>
        </TouchableWithoutFeedback>
    );
};

export default ListItemDeleteAction;