import React from 'react';
import {Text, TextProps} from "react-native";
import {DefaultStyles} from '../../config/defaultStyles';


interface Props extends TextProps {
    children?: React.ReactNode,
    style?: {}
}

const Txt = ({children, style, ...otherProps} : Props) => {
    return (
        <Text style={[DefaultStyles.text, style]} {...otherProps}>{children}</Text>
    );
};

export default Txt;