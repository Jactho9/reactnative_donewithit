import React, {useEffect, useState} from 'react';
import Screen from "../components/Screen";
import * as ImagePicker from 'expo-image-picker';
import Btn from "../components/Btn/Btn";
import {Image} from "react-native";
import ImageInput from "../components/ImageInput";
import ImageInputList from "../components/ImageInputList";

const ImagePickerExample = () => {

    const [imageUri, setImageUri] = useState<string | undefined>();
    const [imageUris, setImageUris] = useState<string[]>([]);

    const requestPermission = async () => {
        const { granted } = await ImagePicker.requestMediaLibraryPermissionsAsync();
        if(!granted)
            alert('You need to enable permission to access the library');
    };

    useEffect(() => {
        requestPermission();
    }, []);

    const selectImage = async () => {
        try {
            const result = await ImagePicker.launchImageLibraryAsync();
            if(!result.cancelled){
                console.log(result.uri);
                setImageUri(result.uri);
            }
        } catch {
            console.log("Error reading image");
        }
    }

    const handleAdd = (uri:string|undefined) => uri && setImageUris([...imageUris, uri]);
    const handleRemove = (uri:string|undefined) => uri && setImageUris(imageUris.filter(imageUri => imageUri !== uri));

    return (
        <Screen>
            <Btn text="Select Image" onPress={selectImage}/>
            <Image source={{uri: imageUri}} style={{width: 200, height: 200}}/>
            <ImageInput imageUri={imageUri} onChangeImage={uri => setImageUri(uri)}/>
            <ImageInputList imageUris={imageUris} onAddImage={handleAdd} onRemoveImage={handleRemove} />
        </Screen>
    );
};

export default ImagePickerExample;