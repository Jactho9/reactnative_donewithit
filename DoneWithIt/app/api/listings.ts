import client from "./client";

const endpoint = "listings";

interface IListingApiImage {
    url: string
    thumbnailUrl: string
}
export interface IListingLocation {
    latitude: number
    longitude: number
}
export interface IListingApi {
    id?: number
    // userId: number
    title: string
    description: string
    price: number
    categoryId: number
    location?: IListingLocation
    images: IListingApiImage[]
}

export interface IFormCategory{
    backgroundColour: string,
    icon: string,
    label: string,
    value: number,
}

export interface IFormListing {
    category: IFormCategory
    description: string
    images: string[]
    location?: IListingLocation
    title: string
    price: string
}

const getListings = () => client.get<IListingApi[]>(endpoint);

const addListing = (listing: IFormListing, onUploadProgress: (progress: number) => void) => {
    const data = new FormData();
    data.append("title", listing.title);
    data.append("price", listing.price);
    data.append("categoryId", listing.category.value.toString());
    data.append("description", listing.description);

    listing.images.forEach((image, index) =>
        data.append("images", {
            // @ts-ignore
            name: "image" + index,
            type: "image/jpeg",
            uri: image,
        })
    );

    if (listing.location)
        data.append("location", JSON.stringify(listing.location));

    return client.post(endpoint, data, {
        onUploadProgress: (progress) =>
            onUploadProgress(progress.loaded / progress.total),
    });
};

export const ListingsApi = {
    getListings,
    addListing,
}