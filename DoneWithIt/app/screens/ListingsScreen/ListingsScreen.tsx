import React, {useEffect} from 'react';
import Screen from "../../components/Screen";
import Styles from "./styles";
import {FlatList} from "react-native";
import Card from "../../components/Card/Card";
import {useNavigation} from "@react-navigation/native";
import {ListingsApi} from "../../api/listings";
import Txt from "../../components/Txt/Txt";
import Btn from "../../components/Btn/Btn";
import ActivityIndicator from "../../components/ActivityIndicator";
import {useApi} from "../../hooks/useApi";

export interface IListing {
    id: number
    title: string
    price: number
    images: any
}

const ListingsScreen = () => {
    const {data: listings, error, loading, request: loadListings} = useApi(ListingsApi.getListings);
    //Can have multiple by just not destructuring

    useEffect(() => {
        loadListings();
    }, []);

    const navigation = useNavigation();

    return (
        <>
            <ActivityIndicator visible={loading}/>
            <Screen style={Styles.screen}>
                {error && (
                    <>
                        <Txt>Couldn't retrieve the listings</Txt>
                        <Btn text="Retry" onPress={loadListings}/>
                    </>
                )}
                <FlatList
                    data={listings}
                    keyExtractor={listing => listing.id?.toString() ?? listing.title + listing.price}
                    renderItem={({item}) => (
                        <Card
                            title={item.title}
                            imageUrl={item.images[0].url}
                            thumbnailUrl={item.images[0].thumbnailUrl}
                            subTitle={"$" + item.price}
                            onPress={() => navigation.navigate("ListingDetails", item)}
                        />
                    )}
                />
            </Screen>
        </>
    );
};

export default ListingsScreen;