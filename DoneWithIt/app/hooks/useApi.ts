import {useState} from "react";
import {ApiResponse} from "apisauce";

export const useApi = <TData>(apiFunc: (...args: any[]) => Promise<ApiResponse<TData>>) => {
    const [data, setData] = useState<TData | undefined>();
    const [error, setError] = useState(false);
    const [loading, setLoading] = useState(false);

    const request = async (...args: any[]) => {
        setLoading(true);
        const response = await apiFunc(...args);
        setLoading(false);

        setError(!response.ok);
        setData(response.data);

        return response;
    }

    return {
        request,
        data,
        error,
        loading,
    };
}