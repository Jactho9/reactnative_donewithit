import React from 'react';
import {Image, ImageSourcePropType, TouchableHighlight, View} from "react-native";
import Swipeable from 'react-native-gesture-handler/Swipeable';

import {ListItemStyles} from "./styles";
import Txt from "../Txt/Txt";
import {Colours} from "../../config/colours";
import {MaterialCommunityIcons} from "@expo/vector-icons";


interface Props {
    title?: string
    subTitle?: string
    image?: ImageSourcePropType,
    IconComponent?: React.ReactNode
    onPress?: () => void
    renderRightActions?: () => React.ReactNode
}

const ListItem = ({title, subTitle, image, IconComponent, onPress = () => console.log("Pressed List Item"), renderRightActions} : Props) => {
    return (
        <Swipeable renderRightActions={renderRightActions}>
            <TouchableHighlight onPress={onPress} underlayColor={Colours.light}>
                <View style={ListItemStyles.container}>
                    {IconComponent}
                    {image && <Image source={image} style={ListItemStyles.image}/>}
                    <View style={ListItemStyles.detailsContainer}>
                        <Txt style={ListItemStyles.title} numberOfLines={1}>{title}</Txt>
                        {subTitle && <Txt style={ListItemStyles.subTitle} numberOfLines={2}>{subTitle}</Txt>}
                    </View>
                    <MaterialCommunityIcons name="chevron-right" size={30} color={Colours.medium}/>
                </View>
            </TouchableHighlight>
        </Swipeable>
    );
};

export default ListItem;