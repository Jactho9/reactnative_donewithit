import {Platform, StyleSheet} from "react-native";
import {Colours} from "../../config/colours";

export const SelectorStyles = StyleSheet.create({
    container:{
        flexDirection: "row",
        alignItems: "center",
        backgroundColor: Colours.light,
        borderRadius: 25,
        padding: 15,
        marginVertical: 10,
    },
    textInput: {
        fontSize: 18,
        fontFamily: Platform.OS === "android" ? "Roboto" : "Avenir",
        color: Colours.dark
    },
    icon: {
        marginRight: 10
    },
    placeholder: {
        flex: 1,
        color: Colours.medium
    },
    text: {
        flex: 1
    }
});

export const SelectorItemStyles = StyleSheet.create({
    text: {
        padding: 20
    }
});
export const SelectorItemCategoryStyles = StyleSheet.create({
    container: {
        alignItems: "center",
        paddingHorizontal: 20,
        paddingVertical: 15,
        width: '33%'
    },
    label: {
        marginTop: 5,
        textAlign: "center"
    }
});