import React, {useState} from 'react';
import Screen from "../components/Screen";
import Selector, {SelectorItemDetails} from "../components/Selector/Selector";
import TxtInput from "../components/TxtInput/TxtInput";

const categories: SelectorItemDetails[] = [
    { label: "Furniture", value: 1 },
    { label: "Clothing", value: 2 },
    { label: "Cameras", value: 3 },
];

const PickerExample = () => {

    const [category, setCategory] = useState<SelectorItemDetails>();

    return (
        <Screen>
            <Selector icon="apps" placeholder="Category" items={categories} selectedItem={category} onSelectItem={item => setCategory(item)}/>
            <TxtInput icon="email" placeholder="Email"/>
        </Screen>
    );
};

export default PickerExample;