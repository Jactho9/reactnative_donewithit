import React, {useState} from 'react';
import {Button, FlatList, Modal, TouchableWithoutFeedback, View} from "react-native";
import {SelectorStyles as Styles} from "./selectorStyles";
import {MaterialCommunityIcons} from "@expo/vector-icons";
import {Colours} from "../../config/colours";
import Txt from "../Txt/Txt";
import Screen from "../Screen";
import SelectorItem, {SelectorItemProps} from "./SelectorItem";

export interface SelectorItemDetails {
    label: string,
    value: number
}

interface Props{
    style?: {}
    icon?: any
    placeholder?: string
    items: SelectorItemDetails[]
    numberOfColumns?: number
    selectedItem: SelectorItemDetails | undefined
    SelectorItemComponent?: React.FC<SelectorItemProps>
    onSelectItem: (item: SelectorItemDetails) => void
    width?: string | number
}

const Selector = ({ icon, items, style, placeholder, numberOfColumns = 1, selectedItem, SelectorItemComponent = SelectorItem, onSelectItem, width = '100%'} : Props) => {

    const [modalVisible, setModalVisible] = useState(false);

    return (
        <>
            <TouchableWithoutFeedback onPress={() => setModalVisible(true)}>
                <View style={[Styles.container, style, {width}]}>
                    {icon && <MaterialCommunityIcons name={icon} size={20} color={Colours.medium} style={Styles.icon}/>}
                    {selectedItem
                        ? <Txt style={Styles.text}>{selectedItem?.label}</Txt>
                        : <Txt style={Styles.placeholder}>{placeholder}</Txt>
                    }
                    <MaterialCommunityIcons name="chevron-down" size={20} color={Colours.medium}/>
                </View>
            </TouchableWithoutFeedback>
            <Modal visible={modalVisible} animationType="slide">
                <Screen>
                    <Button title="Close" onPress={() => setModalVisible(false)}/>
                    <FlatList
                        data={items}
                        keyExtractor={item => item.value.toString()}
                        numColumns={numberOfColumns}
                        renderItem={({item}) =>
                            <SelectorItemComponent
                                item={item}
                                onPress={() => {
                                    setModalVisible(false);
                                    onSelectItem(item);
                                }}
                            />
                        }/>
                </Screen>
            </Modal>
        </>
    );
};



export default Selector;