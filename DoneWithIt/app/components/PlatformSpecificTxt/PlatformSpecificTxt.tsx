import React from 'react';
import {StyleSheet, Platform} from "react-native";
import TxtAndroid from "./TxtAndroid";
import TxtIos from "./TxtIos";
import {Utils} from "../../config/utils";


interface Props {
    children?: React.ReactNode
}

const PlatformSpecificTxt = ({children} : Props) => {
    return (
        Utils.isAndroid
            ? <TxtAndroid>{children}</TxtAndroid>
            : <TxtIos>{children}</TxtIos>
    );
};



const styles = StyleSheet.create({
    text:{
        color: "tomato",
        ...Platform.select({
            ios:{
                fontSize: 20,
                fontFamily: "Avenir"
            },
            android:{
                fontSize: 18,
                fontFamily: "Roboto"
            }
        })
    }
});

export default PlatformSpecificTxt;