import React from 'react';
import {Modal, View, StyleSheet} from "react-native";
import * as Progress from 'react-native-progress';
import LottieView from "lottie-react-native";
import {Colours} from "../config/colours";

interface IUploadScreenProps {
    onDone: () => void
    progress: number
    visible: boolean
}

const UploadScreen = ({progress = 0, visible = false, onDone} : IUploadScreenProps) => {
    return (
        <Modal visible={visible}>
            <View style={styles.container}>
                {progress < 1
                    ? <Progress.Bar progress={progress} color={Colours.primary} width={200}/>
                    : <LottieView
                        autoPlay
                        loop={false}
                        onAnimationFinish={onDone}
                        source={require("../assets/animations/done.json")}
                        style={styles.animation}
                    />
                }
            </View>
        </Modal>
    );
};

const styles = StyleSheet.create({
    container: {
        alignItems: "center",
        justifyContent: "center",
        flex: 1,
    },
    animation: {
        width: 150,
    }
});

export default UploadScreen;