import React from 'react';
import {TextInput, TextInputProps, View} from "react-native";
import Styles from "./styles";
import {MaterialCommunityIcons} from "@expo/vector-icons";
import {Colours} from "../../config/colours";
import {DefaultStyles} from '../../config/defaultStyles';

export interface Props extends TextInputProps{
    style?: {},
    icon?: any,
    width?: number | string
}

const TxtInput = ({ icon, style, width = '100%', ...otherProps} : Props) => {
    return (
        <View style={[Styles.container, style, {width}]}>
            {icon && <MaterialCommunityIcons name={icon} size={20} color={Colours.medium} style={Styles.icon}/>}
            <TextInput
                placeholderTextColor={Colours.medium}
                style={[DefaultStyles.text, {flex: 1}]} {...otherProps}
            />
        </View>
    );
};



export default TxtInput;