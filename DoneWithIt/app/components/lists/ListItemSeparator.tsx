import React from 'react';
import {View} from "react-native";
import {SeparatorStyles} from "./styles";

const ListItemSeparator = () => {
    return <View style={SeparatorStyles.separator}/>;
};

export default ListItemSeparator;