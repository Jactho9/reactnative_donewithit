import React from 'react';
import {View} from "react-native";

const ShadowsExample = () => {
    return (
        <View style={{
            flex: 1,
            alignItems: "center",
            justifyContent: "center",
        }}>
            <View style={{
                backgroundColor: "dodgerblue",
                width: 100,
                height: 100,
                shadowColor: "grey",
                shadowOffset: {width: 10, height: 10},
                shadowOpacity: 0.5,
                shadowRadius: 5,
                elevation: 20,
            }}/>
        </View>
    );
};

export default ShadowsExample;