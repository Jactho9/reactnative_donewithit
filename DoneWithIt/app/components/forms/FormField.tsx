import React from 'react';
import TxtInput, {Props as TxtInputProps} from "../TxtInput/TxtInput";
import { useFormikContext } from 'formik';
import ErrorMessage from "./ErrorMessage";

interface Props extends TxtInputProps{
    name: string,
    width?: number | string
}

const FormField = ({ name, width, ...otherProps} : Props) => {
    const {setFieldTouched, setFieldValue, values, errors, touched} = useFormikContext();
    let values2 = values as { [key: string]: string; };

    return (
        <>
            <TxtInput
                onBlur={() => setFieldTouched(name)}
                onChangeText={text => setFieldValue(name, text)}
                value={values2[name]}
                width={width}
                {...otherProps}
            />
            {/* @ts-ignore */}
            <ErrorMessage error={errors[name]} visible={touched[name]}/>
        </>
    );
};



export default FormField;