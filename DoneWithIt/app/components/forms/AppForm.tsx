import React from 'react';
import {Formik, FormikBag} from "formik";
import {FormikHelpers} from "formik/dist/types";

interface Props{
    initialValues: object
    //onSubmit: (values: any, formikBag?: FormikBag<any, any>) => any
    onSubmit: (values: any, formikHelpers: FormikHelpers<any>) => void | Promise<any>
    validationSchema: any,
    children: React.ReactNode
}

const AppForm = ({initialValues, onSubmit, validationSchema, children} : Props) => {
    return (
        <Formik
            initialValues={initialValues}
            onSubmit={onSubmit}
            validationSchema={validationSchema}
        >
            {() => <>{children}</>}
        </Formik>
    );
};

export default AppForm;