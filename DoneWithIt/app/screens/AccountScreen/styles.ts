import {StyleSheet} from "react-native";
import {Colours} from "../../config/colours";

const Styles = StyleSheet.create({
    container:{
        marginVertical: 20
    },
    screen: {
        backgroundColor: Colours.light
    }
});

export default Styles;