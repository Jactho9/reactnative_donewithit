import React from 'react';
import {View} from "react-native";
import {MaterialCommunityIcons} from '@expo/vector-icons';
import {Colours} from "../../config/colours";

interface Props {
    backgroundColor: string
    iconColor?: string
    name: string//keyof typeof MaterialCommunityIcons.glyphMap
    size?: number
}

const Icon = ({name, size = 40, backgroundColor, iconColor = Colours.white} : Props) => {

    return (
        <View style={{
            width: size,
            height: size,
            borderRadius: size / 2,
            backgroundColor,
            justifyContent: "center",
            alignItems: "center",
        }}>
            {/* @ts-ignore */}
            <MaterialCommunityIcons name={name} color={iconColor} size={size * 0.5}/>
        </View>
    );
};

export default Icon;