import React, {useState} from 'react';
import {NavigationContainer} from "@react-navigation/native";
import navigationTheme from "./app/navigation/navigationTheme";
import AppNavigator from "./app/navigation/AppNavigator";

import { LogBox } from 'react-native'
import OfflineNotice from "./app/components/OfflineNotice";
import AuthNavigator from "./app/navigation/AuthNavigator";
import {AuthContext} from "./app/auth/context";
import {IAuthUser} from "./app/api/auth";
import {AuthStorage} from "./app/auth/storage";
import AppLoading from "expo-app-loading";
import {navigationRef} from "./app/navigation/rootNavigation";
import * as Notifications from "expo-notifications";

LogBox.ignoreAllLogs();

Notifications.setNotificationHandler({
    handleNotification: async () => {
        return {
            shouldShowAlert: true,
            shouldPlaySound: true,
            shouldSetBadge: true,
        };
    },
});

export default function App() {

    const [user, setUser] = useState<IAuthUser>();
    const [isAppReady, setIsAppReady] = useState(false);

    const restoreUser = async () => {
        const user = await AuthStorage.getUser();
        if(user)
            setUser(user);
    }

    if(!isAppReady)
        return <AppLoading startAsync={restoreUser} onFinish={() => setIsAppReady(true)} onError={() => console.log("Error loading app")}/>

    return (
        <AuthContext.Provider value={{user, setUser}}>
            <OfflineNotice/>
            <NavigationContainer ref={navigationRef} theme={navigationTheme}>
                {user ? <AppNavigator/> : <AuthNavigator/>}
            </NavigationContainer>
        </AuthContext.Provider>
    );
}