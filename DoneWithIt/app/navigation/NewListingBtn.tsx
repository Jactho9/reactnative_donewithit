import React from 'react';
import {View, StyleSheet, TouchableOpacity} from "react-native";
import {Colours} from "../config/colours";
import {MaterialCommunityIcons} from "@expo/vector-icons";

interface INewListingBtnProps {
    onPress: () => void
}

const NewListingBtn = ({onPress} : INewListingBtnProps) => {
    return (
        <TouchableOpacity onPress={onPress}>
            <View style={styles.container}>
                <MaterialCommunityIcons name="plus-circle" color={Colours.white} size={40}/>
            </View>
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    container: {
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: Colours.primary,
        borderColor: Colours.white,
        borderWidth: 10,
        height: 80,
        width: 80,
        borderRadius: 40,
        bottom: 20
    }
});

export default NewListingBtn;