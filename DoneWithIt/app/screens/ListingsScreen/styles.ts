import {StyleSheet} from "react-native";
import {Colours} from "../../config/colours";

const Styles = StyleSheet.create({
    screen:{
        padding: 20,
        backgroundColor: Colours.light
    },
});

export default Styles;