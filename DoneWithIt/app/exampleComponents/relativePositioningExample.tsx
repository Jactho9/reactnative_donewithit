import React from 'react';
import {View} from "react-native";

const RelativePositioningExample = () => {
    return (
        <View style={{
            backgroundColor: "#ccc",
            flex: 1,
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "center",
        }}>
            <View style={{
                backgroundColor: "dodgerblue",
                width: 100,
                height: 100,
            }}/>
            <View style={{
                backgroundColor: "gold",
                width: 100,
                height: 100,
                top: 100,
                left: 100
            }}/>
            <View style={{
                backgroundColor: "tomato",
                width: 100,
                height: 100
            }}/>
        </View>
    );
};

export default RelativePositioningExample;