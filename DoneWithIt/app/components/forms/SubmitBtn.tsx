import React from 'react';
import Btn from "../Btn/Btn";
import {useFormikContext} from "formik";

interface Props {
    text: string
}

const SubmitBtn = ({text} : Props) => {
    const {handleSubmit} = useFormikContext();
    return (
        <Btn text={text} onPress={handleSubmit}/>
    );
};

export default SubmitBtn;