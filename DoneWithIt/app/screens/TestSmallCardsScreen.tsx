import React from 'react';
import {StyleSheet, View} from "react-native";
import Screen from "../components/Screen";
import SmallCard from "../components/SmallCard/SmallCard";
import Txt from "../components/Txt/Txt";
import NavigationBar from "../components/NavigationBar/NavigationBar";

const TestSmallCardsScreen = () => {
    return (
        <Screen style={styles.container}>
            <View>
                <Txt style={{marginBottom: 40}}>Test</Txt>
                <View style={styles.cardsContainer}>
                    <SmallCard title={"TEST"} icon={"email"}/>
                    <SmallCard title={"TEST"} icon={"email"} style={{marginHorizontal: 10}}/>
                    <SmallCard title={"TEST"} icon={"email"}/>
                </View>
            </View>
            <NavigationBar/>
        </Screen>
    );
};

const styles = StyleSheet.create({
    container: {
        padding: 10,
        backgroundColor: "#ccc",
        justifyContent: "space-between"
    },
    cardsContainer:{
        flexDirection: "row",
        justifyContent: "space-between",
    }
});

export default TestSmallCardsScreen;