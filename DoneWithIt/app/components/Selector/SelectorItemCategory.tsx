import React from 'react';
import {SelectorItemProps} from "./SelectorItem";
import {TouchableOpacity} from "react-native";
import Txt from "../Txt/Txt";
import {SelectorItemCategoryStyles as Styles} from "./selectorStyles";
import Icon from "../Icon/Icon";

interface Props extends SelectorItemProps {

}
const SelectorItemCategory = ({ item, onPress }: Props) => {
    return (
        <TouchableOpacity style={Styles.container} onPress={onPress}>
            {item && item.backgroundColour && item.icon && <Icon backgroundColor={item.backgroundColour} name={item.icon} size={60}/>}
            <Txt style={Styles.label}>{item.label}</Txt>
        </TouchableOpacity>
    );
};

export default SelectorItemCategory;