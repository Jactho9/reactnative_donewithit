import {Platform, StyleSheet} from "react-native";
import {Colours} from "../../config/colours";

const Styles = StyleSheet.create({
    text:{
        color: Colours.black,
        ...Platform.select({
            ios:{
                fontSize: 20,
                fontFamily: "Avenir"
            },
            android:{
                fontSize: 18,
                fontFamily: "Roboto"
            }
        })
    }
});

export default Styles;