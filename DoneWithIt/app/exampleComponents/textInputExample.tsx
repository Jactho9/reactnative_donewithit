import React, {useState} from 'react';
import Screen from "../components/Screen";
import {TextInput} from "react-native";
import Txt from "../components/Txt/Txt";
import TxtInput from "../components/TxtInput/TxtInput";

const TextInputExample = () => {

    const [firstName, setFirstName] = useState("hi");

    return (
        <Screen>
            <Txt>{firstName}</Txt>
            <TextInput
                maxLength={500}
                keyboardType="numeric"
                clearButtonMode="always"
                placeholder="First Name"
                onChangeText={text => setFirstName(text)}
                style={{
                    borderBottomColor: "#ccc",
                    borderBottomWidth: 1
                }}
            />

            <TxtInput placeholder="Last Name" icon="email"/>
        </Screen>
    );
};

export default TextInputExample;