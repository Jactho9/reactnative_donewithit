import {DefaultTheme} from "@react-navigation/native";
import {Colours} from "../config/colours";

export default {
    ...DefaultTheme,
    colors: {
        ...DefaultTheme.colors,
        primary: Colours.primary,
        background: Colours.white,
    }
}
