import React, {useState} from 'react';
import { Image } from "react-native";
import Screen from "../../components/Screen";
import Styles from "./styles";
import * as Yup from 'yup';
import { ErrorMessage, FormField, SubmitBtn, AppForm} from "../../components/forms";
import {Auth, IAuthCredentials} from "../../api/auth";
import {useAuth} from "../../auth/useAuth";

const validationSchema = Yup.object().shape({
    email: Yup.string().required().email().label("Email"),
    password: Yup.string().required().min(4).label("Password"),
})

const LoginScreen = () => {

    const auth = useAuth();
    const [loginFailed, setLoginFailed] = useState(false);

    const handleSubmit = async ({email, password}: IAuthCredentials) => {
        const result = await Auth.login(email, password);
        if(!result.ok)
            return setLoginFailed(true);
        setLoginFailed(false);
        auth.login(result.data as string);
    }

    return (
        <Screen style={Styles.container}>
            <Image style={Styles.logo} source={require("../../assets/logo-red.png")}/>
            <AppForm
                initialValues={{email: '', password: ''}}
                onSubmit={handleSubmit}
                validationSchema={validationSchema}
            >
                <ErrorMessage error="Invalid email or password." visible={loginFailed}/>
                <FormField
                    autoCapitalize="none"
                    autoCorrect={false}
                    icon="email"
                    keyboardType="email-address"
                    name="email"
                    textContentType="emailAddress"
                    placeholder="Email"
                />
                <FormField
                    autoCorrect={false}
                    icon="lock"
                    name="password"
                    placeholder="Password"
                    secureTextEntry
                    textContentType="password"
                    autoCapitalize="none"
                />
                <SubmitBtn text="Login"/>
            </AppForm>
        </Screen>
    );
};

export default LoginScreen;