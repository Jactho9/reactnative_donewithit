import React from 'react';
import {Text, StyleSheet} from "react-native";


interface Props {
    children?: React.ReactNode
}

const TxtIos = ({children} : Props) => {
    return (
        <Text style={styles.text}>{children}</Text>
    );
};

const styles = StyleSheet.create({
    text:{
        color: "tomato",
        fontSize: 20,
        fontFamily: "Avenir"
    }
});

export default TxtIos;